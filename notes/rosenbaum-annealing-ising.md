T.F. Rosenbaum, J. Brooke, D. Bitko, G. Aeppli  
1999

Quantum annealing of a disordered magnet
----------------------------------------

- simulated annealing represents a connection between stat mech and
  combinatorial optimization
- uses: TSP, circuit design, spin glasses, protein folding
    - have many nearly degenerate local optima
- if annealing is sufficiently slow, annealing is guaranteed to find the
  global optimum (Geman 1984). May be impractical

- atomic spin: magnetic field is perpendicular to spin axis
    - introduces "off-diagonal" terms in Hamiltonian
    - allows tunneling between $\uparrow$ and $\downarrow$

- spin glass (macroscopic lattice of spins) is Feynman's concept of a
  quantum computer
    - input: a spin state
    - output: the lowest-energy spin state
    - in between: a series of quantum-mechanical operations

measuring annealing on spin glass
---------------------------------

- note: this is a physical, not computational, experiment
- use a ferromagnetic material with randomly inserted antiferromagnetic bonds:
  $\ce{LiHo_{x}Y_{1-x}F4}$
- $\ce{Ho}$ is magnetizable, but $\ce{Y}$ is inert: randomizes the coupling
  sign between magnetizable ions

- they use
    $$
    H = - \sum_{i,j} \sigma^{z}_{i} \sigma^{z}_{j} - \Gamma \sum_{i} \sigma^{x}_{i}
    $$
- $[H,\sigma^{z}] \neq 0$ for $\Gamma \neq 0$, so $\sigma^{z}$ isn't
  conserved, and quantum fluctuations arise
- a magnetic field is along $x$: a field in $z$ would polarize spin at all $T$
- sample was
    1. Cooled slowly from 0.8K to 0.03K while unmagnetized; field then
       increased to 7.2kOe
    2. Cooled rapidly from 0.8K to 0.03K while exposed to a 24kOe field; field
       then decreased to 7.2kOe
- samples reached the final conditions in ~18h
- magnetic susceptibility of quantum-annealed magnet remained greater than the
  thermal magnet after 2 days
- relaxation time about 3x better for the quantum experiment
- there's lots of spectra physics here that I don't really follow

Conclusion
----------

- conventional quantum computation approaches involve building computers 1
  qubit at a time using NMR, then manipulating individual qubits for
  computation
- quantum annealing could allow less careful construction: reduce a problem to
  a spin problem and perform thermal/quantum cooling in a magnetic field
