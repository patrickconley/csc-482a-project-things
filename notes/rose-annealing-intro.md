G. Rose, W.G. Macready  
D-Wave  
2007

An Introduction to Quantum Annealing
------------------------------------

- adiabatic quantum computation: computation which adiabatically evolves from
  the ground state of $H_{P}$ to the ground state of $H_{B}$,
  ($[H_{P},H_{0}]\neq 0$).
    - eg. $H = (1 - s) H_{0} + s H_{P}$, $s: 0 \rightarrow 1$
- quantum annealing treats $H_{0} = H'$ as a disordering hamiltonian
  introducing fluctuations which essentially negates the problem hamiltonian.
  System evolves (probably not adiabatically) towards $H_{P}$ until the
  residual energy is low
    - eg. $H = H_{p} + \Gamma H'$, $\Gamma: \Gamma(0) \gg 0 \rightarrow 0$

- physical systems tend to minimize (Helholtz) free energy

- thermal annealing: take a metal with defects (metastable state with
  higher-than-ground energy). Heat & cool it to remove them.
- simulated annealing: heuristic approach for hard problems. Energy landscape
  is user-defined so the ground state is the correct answer
- quantum annealing: in physical (not programmed) systems, can speed up free
  energy minimization (see Santoro 2002)

- when used adiabatically (adiabatic quantum computation), annealing
  processors (eg. D-Wave) are universal quantum computers
- Hydra processors represent optimization of a 2D Ising model in a magnetic
  field:
    - a hard problem
    - represented by (in Einstein notation)
        $$
        H_{0}(t) = h_{i}Z_{i} + h_{ij}Z_{i}Z_{j} + \Delta_{i}(t)X_{i}
        $$
      where $Z_{i}$ and $X_{i}$ are the Pauli spin matrices (per qubit),
      $h_{i}$ is the local bias (what is?), $J$ is the strength of coupling
      between two cubits, and $\Delta$ is the tunnelling matrix ($h$ and $J$
      are programmable)

- Helmholtz energy is
    $$
    F_{T}[p] = E_{p}(V) - TS[p] = \sum_{x}E(x)p(x) - p(x)\ln{p(x)}
    $$
    - at maximum entropy all states are equal
    - V(p) has local minima
    - potential term tries to select state with minimal $V$; entropy term
      tries to eliminate such states
- in classical annealing
    - start at high temperature so entropy term dominates (and global minimum
      is easy to find as F[p] is uniform)
    - cool slowly such that the global minimum in step $t + dt$
        ($T \rightarrow T - dt$) is reachable by gradient of $\frac{dF}{dp}$
        from step $t$
- in simulated annealing:
    - use a Markov chain to sample from $p$ at each step
- in quantum annealing:
    - energy term includes a $K$ contribution which spreads out $p$: exists
      even at low $T$:
        $$
        F_{T}[p] = Tr(\rho V) + \Gamma Tr(\rho K) + T Tr(\rho \ln{\rho})
        $$
    - state selection is from an extra dimension: $(\Gamma,T)$, not simply
      $(T)$

- hard to analyze performance of heuristics (esp annealing) on hard problems:
  input-dependent; time dependent on short-term vs long-term behaviour
    - ultimately need empiric measures of residual energy
    - can measure $E_{res}(t)$: approaches 0 asymptotically
    - on exponential runtime, annealing has residual energy like
        $A log^{-\gamma} \alpha t$, where $\gamma = 2$ for classical annealing
        and $\gamma = 6$ for quantum annealing (see Santoro 2002)
        - probably
    - for $A_{c} \sim A_{q}$, $\alpha_{q} \sim 10^{9}$,
        $\alpha_{c} \sim 10^{3}$ (estimated), quantum annealing beats
        simulated annealing by $10^{6}$
    - however, most problems have specific algorithms running faster than a
      general annealing algo, so relative performance of QA is hard to define
