A.B. Finnila et al.  
1994

Quantum annealing: a new method for minimizing multidimensional functions
-------------------------------------------------------------------------

- some methods
    - single-variable functions with unknown derivative: Brent's method,
      golden section search
    - multivariate functions with known 1st derivative: conjugate gradient,
      variable metric
    - otherwise: downhill simplex, direction-set, simulated annealing, genetic
      annealing.
- annealing:
    parallelizable (as with other Monte Carlo methods)
- quantum methods known in protein folding: tunnel out of, or smooth away,
  local minima
- (Amara, 1993) developed a minimization technique on multi-D potential by
  approximating the imaginary-time Schrödinger equation as the product of
  Hartree single-wave packets: packets can separately move, expand, contract,
  tunnel
- quantum annealing doesn't require approximating Schrödinger

- in general, we can start at an arbitrary $(\Gamma,T)$ point and reduce to
  $(0,0)$ along any path. It happens to be easier to find the thermal ground
  state of a quantum system, though, so anneal $T$ first.

- for reference, the i-time Schrödinger is
    $$
    \frac{\partial \psi}{\partial \tau} =
        \frac{\hbar^2}{2m}
        \frac{\partial^2 \psi}{\partial x^2}
        - \left[ V(x) - E_0 \right] \psi
    $$
    ($\tau = \frac{it}{\hbar}$, $E_0$ is a convenient constant)
    - solution has the form
        $$
        \psi(x,\tau) = \sum_n \phi_n(x) \exp^{ - \left( E_n - E_0 \right) \tau }
        $$
        (eigenfunctions $\phi(x)$ with energies $E_n$)
        - ground state is the large-$\tau$ limit of the wave function; has
          energy $E_0$
- Diffusion Monte Carlo can be used: perform random walks through the solution
  space
    - walkers diffuse by nature
    - where $V(x) > E_0$, walkers decay
    - where $V(x) < E_0$, walkers grow
- DMC needs no knowledge of wave function: it is obtained from the walkers'
  distribution
- rate of convergence is controlled by $E_n - E_0 = \Delta E$; DMC converges
  in $\frac{\hbar}{\Delta E}$, which is the tunneling time scale between
  interacting optima in the potential
- anneal by gradually increasing the mass of the walkers

> example:
    - $V(x) \sim x^4$ with two minima ($s_2$ slightly lower than $s_1$)
    - start a bunch of walkers in $s_1$ with energy around half the barrier
      height.
    - diffuse of walkers to an equilibrium state causes a slight majority of
      them to tunnel into $s_2$ (Fig 2: walkers have $m_{walker} \sim 2000
      m_p$, barrier is 1K).
    - anneal the system by increasing walker mass: walkers initially have high
      energy and move easily between states; as mass increases, ground state
      energy decreases and walkers find it hard to move from $s_2$ into $s_1$.
      (Fig 3: walkers have $m_{walker} \sim 200,2000,2000000 m_p$).

- as ground state energy drops below the energy of a metastable state it
  becomes impossible to move into that state: slow evolution is necessary so
  we don't drop out the bottom
- at each MC step, the system must diffuse into (or close to) an equilibrium
  so walkers can be said to be in a state

- walkers ← delocalization
