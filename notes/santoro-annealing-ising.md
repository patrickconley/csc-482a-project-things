G.E. Santoro, R. Martoňák, E. Tosatti, R. Car  
2002

Theory of Quantum Annealing of an Ising Spin Glass
--------------------------------------------------

- CA is used in microchip design
- Ising experiments use $\ce{LiHo_{0.44}Y_{0.56}F4}$
- $\Gamma$ is a transverse magnetic field: it mixes $S^{z}$: at time 0 it
  completely disorders the system
- it's not entirely clear from a theoretical perspective why QA should be
  faster than CA
    - it's not even clear how CA works: experiments show decay faster than
      theory
- time is represented as number of Monte Carlo steps, as an actual
  (exact?) annealing would be too slow

- 2D random Ising model is technically polynomial, but prohibitively
  expensive
- $H = \sum_{ij} J_{ij} \sigma^z_i \sigma^z_j - \Gamma \sum \sigma^x_i$
    - $J$ represents the coupling between elements of the lattice
    - transverse field $\Gamma$ causes transitions between $\uparrow$ and
      $\downarrow$
- $\Gamma_0 \Rightarrow$ disordered quantum paramagnet

- QA system uses:
    - small constant temperature $T$
    - represented on a classical model using $P$ Trotter replicas of the
      lattice: $PT \sim 1$ seems to be a good choice. Runtime is linear in
      $P$; ceases to be an improvement beyond a certain point
    - $\Gamma$ changes the coupling between replicas: at high $\Gamma$ they're
      loosely coupled; as $\Gamma$ decreases the coupling becomes tighter
      until they're forced into identical configurations

- ground-state transitions are tunnelling events between a wide, shallow
  valley and a deep, narrow valley

- accuracy is logarithmic, but at a higher power than CA: it converges more
  quickly, but will not converge adiabatically in polynomial time (cannot be
  used to precisely solve NP-complete problems)
- PIMC is parallelizable, unlike CA algorithms
