PARTIAL
=======

S. Geman, D. Geman  
1984

Stochastic Relaxation, Gibbs Distributions, and the Bayesian Restoration of Images
----------------------------------------------------------------------------------

- at low temperature state transitions will tend to decrease the cost
- at high temperature transitions occur essentially at random

- Metropolis at $T = 0$ is greedy: gradient descent
- Metropolis at $T = \infty$ is purely random

- annealing begins as a mostly-random process and becomes more like iterative
  improvement as time passes
- adiabatic annealing guarantees convergence, but is slow. An adiabatic
  schedule can be used to describe an appropriate form for faster annealing
