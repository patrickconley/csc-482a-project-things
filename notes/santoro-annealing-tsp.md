R. Martoňák, G.E. Santoro, E. Tosatti  
2004

Quantum annealing of the Traveling Salesman Problem
---------------------------------------------------

- statistical annealing: minimize costs through an analogy to statistical
  mechanics
    - simulation via Monte Carlo or molecular dynamics
    - unbiased, unlike gradient methods
- quantum annealing: minimize through quantum mechanics
    - quantum kinetic rather than thermal fluctuations
    - quantum fluctuations are the reason helium melts at low temperatures
    - QFs can tunnel through potential barries. SA can overcome them, but
      possibly slower
    - from (Finnila 1994)
    - Santoro found QA faster on 2D random Ising

- TSP: they (everyone?) use a Hamiltonian cycle, not path
    - SA is not the best, but it is flexible and simple
- we must define the Hilbert space and the quantum kinetic hamiltonian
- they map it to a constrained Ising-like system reminiscent of neural network
  approaches (Hopfield-Tank 1986)

- define a permutation matrix $T$: entry $T_{ij}$ is true iff a tour follows
  $(i,j) \in E$
    - irreflexive; each row and each column contains one true
      element
    - as reversed tours have the same length, use $U = T + T'$ (symmetric)
    - $H_{V} = \frac{1}{2} d_{ij} U_{ij}$
    - use 2-opt move:
        $(c_1, c_2), (c'_1,c'_2) \rightarrow (c_1,c'_1), (c_2,c'_2)$
      and reverse the intermediate links
- in $T$, the reversal amounts to a bunch of bit- (spin)-flips: non-local
    - $U$ is directionless, though, so the operation only requires four
      flips:
        $$
        S^{-}_{<c_{1},c_{2}>} S^{-}_{<c_{1'},c_{2'}>}
        S^{+}_{<c_{1},c_{1'}>} S^{+}_{<c_{2},c_{2'}>}
        $$
      where $S^{z}_{<i,j>} = 2U_{i,j} - 1$

> eg. $(1,2,3,4,5,6,7,8) \rightarrow (1,2,7,6,5,4,3,8)$
> - replace two links $(2,3)$ and $(7,8)$ with $(2,7)$ and $(3,8)$, and
>   reverse all intermediate links

- then the hamiltonian is
    $$
    \begin{align}
    H_{V} & = \sum_{<ij>} d_{ij} \frac{S^{z}_{<i,j>} - 1}{2} \\
    H_{K} & = \frac{1}{2} \sum_{<i,j>}\sum_{<i',j'>} \Gamma(i,j,i',j',t)
        \left[
        S^{-}_{<c_{1},c_{2}>} S^{-}_{<c_{1'},c_{2'}>}
        S^{+}_{<c_{1},c_{1'}>} S^{+}_{<c_{2},c_{2'}>} + H.c
        \right]
    \end{align}
    $$
- $\Gamma$ is a decaying function of time biased towards selecting cities
  close to one another (*neighbourhood pruning*)
- it is now a constrained Ising problem: $H_{V}$ represents the external
  magnetic field; the problem has fixed magnetization ($N$ elements)
- use heuristic approximations to solve the Schrödinger equation (as
  it's exponential in the number of variables): quantum Monte Carlo
  (specifically path-integral Monte Carlo)
    - this is still hard: have to calculate $<C' | exp - \epsilon H_{K} | C>$;
        Trotter discretization is complicated
    - simplify: $H_{K} = \Gamma(t) \sum_{<ij>} 
        \left[ S^{+}_{<i,j>} + H.c \right]$:
        represents an Ising system in a transverse magnetic field
    - does not guarantee the validity-constraint, but that can be enforced in
      the MC.
- remaining implementation is as for random Ising model (Santoro 2002)

- they compare against CA using TSPLIB (instance pr1002): $N = 1002$ cities,
  optimal length known to be $L_{opt} = 259045$
    - use Metropolis algo: select $T_{0}$ by performing several short runs
      starting at high temperature (ergodic region) to identify the point
      (*dynamical temperature*) where different runs tend to diverge:
      $T_{CA} = 100$; $T_{QA} = 10/3$ (constant); 
    - at each MC step they select a city $j$ and examine its $M = 20$ nearest
      neighbours for 2-opt candidates: each MC step has $MN$ attempted moves
    - quality based on average cost across 96 runs
- found residual error is reduced much more quickly for QA than for SA
    - very low residual error is obtained faster for QA even after accounting
      for extra CPU time (Ising simulation uses 30 classical ferromagnets
      rather than one quantum magnet)
    - they've hypothesized on Ising models than QA is more efficient as it
      overcomes infinite potential barriers---appears to apply here too
    - QA saturates, however, so extremely low error is hard to obtain
