\documentclass[10pt,onecolumn,oneside,notitlepage]{article}
\usepackage{xcolor,graphicx,caption,subcaption,fixltx2e,amsmath}
\usepackage[pdftex]{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{cite}
\usepackage{amssymb}
\usepackage{url}
\usepackage[version=3]{mhchem}
\usepackage{braket}

\title{Quantum Annealing}
\author{Justin Gaudet \\ Patrick Conley}

\begin{document}

\maketitle

\begin{abstract}
   The goal of this project is to look into the feasibility of QA algorithms
   in terms of classical computer science problems. We discuss how QA is
   modelled, and how a hard problem can be simulated using QA\@. This was simply
   a research project in that we were not able to fully implement any of the
   algorithms or processes described. We analyze QA and provide analogies to
   its properties. We describe how Ising spin glass, TSP and 3-SAT can be
   modelled on a quantum machine, and provide a comparison to classical
   methods and simulated annealing methods of solving these problems.
\end{abstract}

\section{Introduction}

Heuristic optimization methods (or, more specifically, metaheuristic
methods\cite{WP-metaheuristic}) are an important class of approximation
algorithms for hard problems, designed to converge gradually toward a problem
instance's optimal solution. Standard approximation algorithms are
characterized by a definite running time and an approximation ratio---the
guaranteed quality ratio of an approximate solution to the optimal solution,
whereas heuristic algorithms are described by a residual error. Reducing this
expected residual error on a problem is equivalent to increasing the
algorithm's running time.

Among the simplest heuristic algorithms are gradient descent methods, which
calculate the change in cost over a small region of the solution space (the
gradient of the problem's cost function), and iterate to new regions by
following the gradient toward the optimum\cite{WP-gradient_descent}. These
heuristics are only useful on simple systems, however: univariate functions,
or differentiable multivariate functions\cite{FinnilaQA}. More complex
functions, especially those with many local optima cannot be effectively
solved using gradient-based algorithms. These algorithms run a risk of
becoming trapped in local optima, which may be nearly degenerate with, or much
worse than, the global optimum\cite{SantoroReview}.

Other heuristic algorithms are better equipped for such frustrated systems, at
the cost of increased complexity. Quantum annealing is one such algorithm. It
is most easily described by analogy to simulated annealing, which is in turn
described by analogy to thermal annealing in statistical
mechanics\cite{KirkpatrickSA, BrookeIsing, MartonakTSP}.

With these processes, one can model classical hard problems in computer
science, such as the Travelling Salesman Problem and 3-SAT\@. The optimal
solutions can be simulated using quantum techniques that have an equivalent
meaning in the classical sense (such as 2-OPT, city-swap, and quantum
tunnelling)\cite{BonomiTSP}.

While simulated annealing has been a long-time contender for an optimal method
of solving TSP, it has never outperformed certain ad-hoc methods or tabu
search. Quantum annealing, however, has the special property that it can
escape local minima of an objective function by “tunnelling” through to a new
local cost\cite{WP-quantum_annealing}. This greatly improves running time and
the quality of the calculated solution. While still not better than heuristic
approaches, QA comes very close to being one of the most efficient methods for
solving TSP and is a feasible technique on both simulations and adiabatic
computers.

\section{Background}

The total energy of any system can be described by its Hamiltonian, $H = V + K$.
In the case of a newly-refined metal (or other crystalline solid), the
potential energy $V$ is determined by the chemical bonds between molecules in
the material: the more tight the bond between two molecules, the lower the
material's potential. The kinetic energy $K$ is a function of the material's
temperature, or entropy (if the problem is reformulated in terms of Helmholtz
free energy\cite{DWaveIntro}). At high temperature, the temperature/entropy
dominates and smooths out energy differences between different potential
energy minima\cite{DWaveIntro}, allowing the material to move freely into any
state; at low temperature the potential energy begins to take over and forces
the material into a stable or metastable state (ie., solidify). However, not
all stable states are exhibit the same properties. What we wish to analyze are
states with energy near the ground state. These are crystalline with few
defects and are relatively strong\cite{WP-simulated_annealing, WP-annealing}.
Such states are typically rare, however\cite{KirkpatrickSA}: the energy
landscape is dominated by disordered (amorphous) metastable states called
glasses.

Thermal annealing is a process of slowly cooling the material from its liquid
state. If the material is cooled quickly, thermal energy is transferred as
heat to the material's chemical bonds, and it settles into a high-energy glass
state\cite{KirkpatrickSA}. If the cooling is sufficiently slow (ie., close to
adiabatic), then the material reaches an equilibrium state via a gradient from
the global ground state\cite{DWaveIntro}.

Simulated annealing is simply an abstraction of thermal annealing. The state
of a system being annealed is interpreted as the state of a finite Markov
chain\cite{DWaveIntro}; the temperature is replaced with a pseudotemperature
which decays slowly from a high initial value to 0; the potential energy $V$
is replaced with a cost function based on the problem being studied. It may
represent the length of a tour in the travelling salesman
problem\cite{MartonakTSP}, the number of unsatisfied clauses in a
satisfiability problem\cite{Battaglia3SAT}, the number of connections between
chips on a circuit board design\cite{KirkpatrickSA}, or the coupling energy
between atomic spins in a paramagnetic lattice\cite{KirkpatrickSA,
BrookeIsing, SantoroIsing}. The latter problem is particularly important for
quantum annealing.

Simulated annealing operates on a modification of the Metropolis algorithm, an
early Monte Carlo method. At each step of a simulation, a neighbouring state
$s'$ of the current state $s$ is identified, and transitions to the new state
probabilistically, based on $P(\Delta E, T)$\cite{KirkpatrickSA}. The typical,
and original, transition probability distribution is
\begin{equation*}
   P(\Delta E, T) = \exp(-k \Delta E / T)
\end{equation*}
Simulated annealing modifies the Metropolis algorithm by reducing the value of
$T$ after each step (annealing) at a constant rate $\frac{dT}{dt}$, whether or
not a state transition occurs. By including a nonzero probability of making a
transition to a state at higher potential than the current state, the
algorithm is able, at any $T > 0$, of climbing out of any finite local
minimum, thus avoiding the “trapping” suffered by gradient descent methods.

Simulated annealing is guaranteed to find the global minimum to a problem in
which all states have finite cost\cite{GemanSA}, but only if the annealing
schedule used is nearly adiabatic. This is prohibitively long for hard
problems, but it is typically possible empirically to identify faster
annealing schedules with similar functional properties to the adiabatic
schedule\cite{GemanSA, BrookeIsing, SantoroIsing}. Additionally, it remains
vulnerable to problems with prohibited states (which represent infinite
potential, and cannot be climbed across\cite{SantoroReview}), and to strongly
frustrated systems: problems of high dimensionality, with a global minimum
nearly degenerate to a large number of local minima\cite{SantoroReview}.
Quantum annealing is able to avoid both these problems by incorporating
delocalization and tunnelling.

Quantum annealing represents a further modification of thermal annealing. The
problem's Hamiltonian is modified to include an additional kinetic term: $H =
V + K_{therm} + K_{kin}$. The new kinetic term is inspired by quantum
mechanics, representing the superposition of all of the system's states. (As
quantum superpositions are difficult to represent in classical simulations,
$K_{kin}$ is implemented as a coupling between a large number of replicas of the
problem---typically 20--30\cite{SantoroIsing, FinnilaQA}---which are simulated
in parallel.) The quantum kinetic term contains a source of quantum
fluctuations, of strength $\Gamma_{0}$, which decays over the course of the
annealing process. The goal of annealing, Then is to move from an arbitrary
point in $(\Gamma,T)$ space to $(0,0)$\cite{FinnilaQA}. In principle annealing
can progress along any path, but in practise it is more efficient to first
perform classical annealing to $(\Gamma_{0},P^{-1})$ (where $P$ is the number of
problem replicas).

\section{Discussion}

\subsection{An illustrative example}
\label{sec:simple-ex}

An example, drawn from A.B. Finnila's development of the method, helps to
clarify the process\cite{FinnilaQA}. Consider a system in a potential well
with two minima; the right minimum has a potential energy slightly lower than
the left minimum. A number of particles are placed in the left minimum
(Figure~\ref{fig:Finnila-diffusion}, line $a$); the kinetic energy $E_{n}$ of the
particles is approximately half the energy of the barrier. The particles are
allowed to diffuse through a Diffusion Monte Carlo process: each particle
moves to a new state $x$ according to a random walk; it is then duplicated or
destroyed with probability proportional to $E_{ref} - V(x)$, where $E_{ref}$
is a reference energy adjusted after each step to maintain a constant
population\cite{AndersonDMC}. Annealing begins when the particles enter an
equilibrium state (according to their normalized distribution:
Figure~\ref{fig:Finnila-diffusion}, line $d$ or
Figure~\ref{fig:Finnila-annealing}, line $a$). The energy of the particles is
decreased by a small amount $dE$, and the particles diffuse to a new equilibrium
(for sufficiently small $dE$ a single diffusion step is sufficient). In the
final state $E_{0}$, the particles' kinetic energy is less than the potential
of the left minimum, and all surviving particles are in the ground state
(Figure~\ref{fig:Finnila-annealing}, line $c$).

The diffusion process, even in this simulated case, is described as using
quantum tunnelling, as there remains a nonzero probability of finding a
particle in the prohibited states near the barrier between the left and right
minima, where the background potential exceeds the energy of the diffusers.
The equilibrium distribution, at any $E_{i}$, is equivalent to a numerical
solution for the wave function of a single particle in the $i^{\mathrm{th}}$
excited state\cite{FinnilaQA}, although it is not necessary during the
annealing process to explicitly solve the Schr{\"o}dinger equation and find
the wave function\cite{FinnilaQA}.

\begin{figure}[tb] \centering
   \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[angle=0,width=\textwidth]{images/finnila2.png}
      \caption{Diffusion~\label{fig:Finnila-diffusion}}
   \end{subfigure}
   \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[angle=0,width=\textwidth]{images/finnila3.png}
      \caption{Annealing~\label{fig:Finnila-annealing}}
   \end{subfigure}
   \caption{Simplified example of quantum annealing.
      \subref{fig:Finnila-diffusion}: diffusion of small masses in a potential
      well (solid line) at constant $(\Gamma,T)$, from an initial state at $a$
      to an equilibrium state at $d$. \subref{fig:Finnila-annealing}: quantum
      annealing of the same masses from an initial state at $a$ to ground
      state at $c$\cite{FinnilaQA}.~\label{fig:Finnila-example}
   }
\end{figure}

\subsection{The Ising spin glass problem}

All fundamental particles possess a property called ``spin'', which acts as a
quantum-mechanical analogue to the angular momentum of a rotating
object\cite{WP-spin}. In a ferromagnetic material, such as iron, or
the rare earth metal \ce{LiHoF4}, it is the alignment of quantum spins that
cause the material's magnetization. For simplicity, we will consider only spin
in the $z$-direction, given as $\sigma^{z}$. The spin of a particle can have
only one of two values, $\Ket{\uparrow}$ and $\Ket{\downarrow}$; a particle will
normally exist in a superposition of these states.

In a spin glass, some of the ferromagnetic atoms in a 2D lattice are replaced
with non-magnetic ones: $\ce{LiHo_{x}Y_{1-x}F4}$. The result contains
antiferromagnetic bonds, which bias some atoms toward antiparallel spins, and
a much more complicated arrangement of chemical bonds\cite{BrookeIsing}:
the ground state (the arrangement with greatest magnetic susceptibility)
becomes difficult to calculate. If a sample of such a material is placed in a
magnetic field aligned with the $x$ axis, the field will act as a source of
quantum fluctuations by attempting to align $\sigma^{z}$ in a prohibited
direction.  The system has the relatively simple Hamiltonian
\begin{equation*}
   H = \sum_{i,j} J_{i,j} \sigma^{z}_{i} \sigma^{z}_{j} - \Gamma \sum_{i}
   \sigma^{x}_{i}
\end{equation*}
where $J_{ij}$ is the coupling between atoms in the lattice, and $\Gamma$ is
related to the magnetic field strength. At high $\Gamma$, the glass acts as a
paramagnet\cite{SantoroIsing}: it can be magnetized by the presence of a field
in $z$, but does not retain its magnetization.

Quantum annealing can be performed on an Ising glass in one of two ways:
directly, or in a simulation. In the direct case, quantum annealing ceases to
be an analogy: a sample of $\ce{LiHo_{0.44}Y_{0.56}F4}$ is created in the
laboratory, and placed in a strong magnetic field. It is cooled rapidly to
near 0K (thermal annealing), and the magnetic field is slowly reduced (quantum
annealing). Figure~\ref{fig:ising-physical} compares the magnetic
susceptibility of an Ising glass annealed under a pure thermal regime (with a
weak magnetic field) and under a mixed thermal \& quantum regime.  A sample
using quantum annealing converged to a much higher magnetic susceptibility
after a short annealing schedule.

\begin{figure}[tb] \centering
   \includegraphics[angle=0,width=0.6\textwidth]{images/rosenbaum.png}
   \caption{Comparison of thermal annealing (blue) and thermal/quantum
      annealing (red) of an Ising spin glass, $\ce{LiHo_{0.44}Y_{0.56}F4}$:
      temperature (top), applied magnetic field (centre), and the sample's
      magnetic susceptibility (bottom)\cite{BrookeIsing}.~\label{fig:ising-physical}
   }
\end{figure}

The spin glass can additionally be simulated using a process similar to
Section~\ref{sec:simple-ex}. As the 2D Ising model can be easily represented
as a boolean matrix containing the atoms' spins, superposition is achieved by
creating a number of replicas (called Trotter replicas) of the original
model\cite{SantoroIsing}. These replicas form a 3D model, with elements
coupled to each other by $J_{ij}$ in the physical dimensions and by $\Gamma$
in the Trotter dimension, such that the replicas are initially only weakly
coupled, and are forced into a common configuration when fully annealed.
The wave function is calculated at each step using Path Integral Monte Carlo,
rather than DMC, although the two formulations are equivalent\cite{KosztinDMC}.

The Ising spin problem is a useful problem for quantum annealing, first
because it is often straightforward to represent other problems, such as the
travelling salesman problem, or boolean satisfiability, as 2D boolean
matrices; second, because Ising spin glasses can be constructed physically,
either imprecisely using chemical methods, or with semi-precise means using
NMR and STM\cite{BrookeIsing}.

\subsection{The travelling salesman problem}

To model the Travelling Salesman Problem with quantum annealing, we start by
defining the shortest tour for a graph $G$ as $H_{TSP}$\cite{MartonakTSP}. This
is defined by the Hamiltonian describe in the previous section, where $H_{tsp}
= H_{pot} + H_{kin-}$ describes the potential energy Hamiltonian plus the
artificial kinetic energy Hamiltonian (introduced to the system in the form of
quantum fluctuations). If $G$ has $N$ cities, then we define $U$, an $N \times
N$ matrix of 0's and 1's, such that $U_{ij}$ is 1 if city $i$ is connected to
city $j$ and 0 otherwise. The solution to this Hamiltonian can be defined as
$S$. The potential energy Hamiltonian is defined as follows: $H_{pot}(U) =
\frac{1}{2} \sum_{ij}d_{ij}U_{ij}$ where $d_{ij}$ is the distance between city $i$
and city $j$. The kinetic energy $H_{kin}$ is defined in terms of the 4 Ising
spin-flip operators\cite{SantoroReview}.

$S$ begins as a candidate solution, and as the system is cooled, we look for
better solutions (minima) in the neighbourhood of $S$, $S'$ based on the
current $U$. At each pseudotemperature, we look for a fixed number of
solutions in the neighbourhood, as defined by our Monte Carlo step size. At
each step, the neighbourhood $S'$ is all solutions that can be obtained by a
trivial operation to $S$. In this case, a trivial operation to the graph $G$
is any 2-Opt move\cite{WP-2_opt}. However, this implies that two cities with large
inter-city distance could be operated on, producing large overhead in finding an
optimal solution.  Neighbourhood pruning\cite{JohnsonTSP} is done on $S'$ to
avoid this problem; long distance paths will not be flipped in a 2-Opt move.
The equivalent to this 2-Opt operation is represented by the 4 Ising
operators: $S^{\mathbb{Z}}_{\left< i,j \right>} = 2U_{ij} -1 \in \{-1,1\}$.

Once we finish cooling the system, and it has reached a ground state
equilibrium, then the path described by U will be the optimal tour. It was
found that this QA approach to solving the TSP gives on average a 1.57\%
increase in path length over the Lin-Kernighan algorithm\cite{MartonakTSP,
WP-lin_kernighan}, and a much greater improvement over simulated annealing
(see Figure~\ref{fig:TSP-residue}).

\begin{figure}[tb] \centering
   \includegraphics[angle=0,width=0.8\textwidth]{images/martonak-excess.png}
   \caption{Average residual excess length\cite{MartonakTSP}.}~\label{fig:TSP-residue}
\end{figure}

\subsection{Boolean 3-satisfiability}

In the typical 3-SAT problem, the goal is to maximize the number of satisfied
clauses in some boolean formula. In the QA version of 3-SAT, we look at the
contrapositive of the typical 3-SAT problem: we wish to minimize the number of
violated clauses with some assignment to the boolean variables.

To model the boolean clauses in QA, we associate each boolean variable with an
equivalent Ising spin variable (as mentioned in previous sections):
$S^{\mathbb{Z}}_{i,j}$ for the $i^{\mathrm{th}}$ boolean variable in the
$j^{\mathrm{th}}$ clause. Each clause containing 3 boolean variables ($i$,$j$,
and $k$) is then assigned a computed energy value defined as
follows\cite{Battaglia3SAT}:
\begin{equation*}
   E_{a} = \frac{1}{8} (1+J_{ai}S_{i})(1+J_{aj}S_{j})(1+J_{ak}S_{k})
\end{equation*}
Here, $J_{an}$ takes on the value $-1$ if the boolean variable $n$ is negated
in clause $a$ and $+1$ otherwise. The Hamiltonian over $M$ clauses then
becomes:
\begin{equation*}
   H_{classical}(\{Si\})= \sum_{a=1}^{M}E_{a}
\end{equation*}
which takes an Ising spin operator as parameter.

To simulated this on a quantum machine, we turn this Hamiltonian into
a Path-Integral Monte Carlo (PIMC) scheme. We replace the Ising spin
operator with a Pauli \verb+SU(2)+ spin operator $\sigma$ and introduce a
perturbing transverse field $\Gamma$. The Hamiltonian we wish to solve then
becomes:
\begin{equation*}
   H_{quantum} = H_{classical}(\{\sigma_{i}^{\mathbb{Z}}\})
   - \Gamma \sum_i \sigma_{i}^{\mathbb{Z}}
\end{equation*}

The performance of annealing on 3-SAT is parameterized by the
residual energy once the system has reached equilibrium, that is, the
difference in energy between the system in its final state and the
energy level of the known ground state. We can then compare the
performance between PIMC-type algorithms and classical annealing
algorithms (see Figure~\ref{fig:3SAT-residue}).

\begin{figure}[tb] \centering
   \includegraphics[angle=0,width=0.6\textwidth]{images/battaglia-excess.png}
   \caption{Performance comparison between classical annealing (CA), PIMC,
      PIMC with global jumps, and a lower bound Gardner
      Energy\cite{Montanari3SAT}.~\label{fig:3SAT-residue}
   }
\end{figure}

\section{Conclusions}

We have looked at the differences between simulated annealing and quantum
annealing, and how they can be used to solve a variety of hard problems. While
quantum annealing is still a fairly new concept, it is already proving to be a
feasible method of solving these hard problems. Both simulated annealing and
quantum annealing are still, however, lacking in performance when compared to
other heuristic or ad-hoc methods of solving both TSP and 3-SAT\@. We have also
looked at the specifics of how QA can be modelled to solve instances of TSP and
3-SAT\@.

The performance of QA, while still better than SA, has a long way to go before
it can be used with commercial problem instances. Currently, adiabatic systems
that can run these complex simulations, are about the same computational power
of a modern-day laptop. However, with companies like D-Wave making progress on
both the systems themselves, but also the algorithms designed to run on
quantum adiabatic systems, we are expected to see a jump in performance,
putting QA on the shortlist for methods of solving hard problems. Adiabatic
quantum computation has the advantage over ``classic'' quantum computers based
on quantum gate arrays that an AQC does not require the careful placement of
each qubit: an error-prone Ising glass can be used for computation by shifting
the positions of antiferromagnetic bonds.

\bibliographystyle{IEEEtran}
\bibliography{annealing}

\end{document}
