SHELL = /bin/bash

INPUT = $(wildcard notes/*.md)
OUTPUT = $(addprefix html/,$(notdir $(INPUT:.md=.html)))

all: $(OUTPUT)

.PHONY: paper
paper: paper.tex annealing.bib
	latexmk -pdf -use-make -pdflatex="pdflatex -interaction=nonstopmode -shell-escape" $<

.PHONY: index
index:
	rm -f html/index.html README.md
	for file in $(notdir $(OUTPUT:.html=)); do \
		echo "<a href='$$file.html'>$$file</a><br>" >> html/index.html; \
		echo "[$$file](html/$$file.html)  " >> README.md; \
	done

html/%.html: notes/%.md | html
	mathjax --chem -- $< > $@

html:
	mkdir $@

clean:
	latexmk -c
